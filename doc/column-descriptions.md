# Primer Candidate Column Header Descriptions


## ID
  A compound key, consisting of three fields separated by a tilde ('~')
  character. The first field is the NCBI accession number for the
  genome, with coordinates for the template used for initiating the
  primer search. The second field is the strand from which the primer
  was derived. The third field is an ordinal identifier distinguishing
  the primer-probe set from others found on the same genome and the
  same strand.
  
  
## FWD Primer
  The forward (or 'left') primer of the primer-probe set. It is
  written in the 5' -> 3' direction, on the sequence and strand
  identified in the ID column.
  
## REV Primer

  The reverse (or 'right') primer of the primer-probe set. It is
  written in the 5' -> 3' direction, on the same sequence, but
  *opposite* strand, as the corresponding forward primer as found in
  the 'FWD Primer' column.
  
## Probe

  The hybridization probe of the primer-probe set. It is written in
  the 5' -> 3' direction, on the same sequence and strand as the
  forward primer, as found in the 'FWD Primer' column.
  
## \# oligo FP
  The number of times any of the oligos of the primer-probe set (i.e.,
  the forward or reverse primer sequences, or the hybridization probe)
  bind to off-target genomes.

## \# amplicon-only FP hits
  The number of times that the forward and reverse primers are found
  in off-target genomes, such that they form an amplicon. Does not
  consider the probe sequence.
  
## \# hits
  The number of times that the forward and reverse primers, *and* the
  probe sequence (i.e., the primer-probe set), are found together among
  all complete genomes (i.e., off- and on-target genomes).
  
## \# TP
  The number of on-target genomes that have at least one copy of the
  primer-probe set.
  
## \# FP
  The number of off-target genomes that have at least one copy of the
  primer-probe set.
  
  
## \# FN
  The number of on-target genomes where the primer-probe set is not
  found.
  
## Precision
  A measure of how often positive predictions are correct; ranges from
  0 (bad) to 1 (good).
  
## Recall
  A measure of how often correct results are found by the predictions;
  ranges from 0 (bad) to 1 (good).
  
## F1 Score
  A combination of Precision and Recall to provide an accuracy-like
  score; ranges from 0 (bad) to 1 (good).
  
## Avg mismatches
  The sum of all primer-probe set mismatches among TP alignments,
  dividied by the number of TP hits.
  
## Avg extra amplicons
  The average number of amplicons found among target genomes, beyond
  the one expected.
  
## Avg extra probe hybs
  The average number of hybridization probe binding sites found among
  target genomes, beyond the one expected.

## FWD Tm
  The melting temperature (in degrees Celsius) of the forward primer.

## REV Tm
  The melting temperature (in degrees Celsius) of the reverse primer.

## Primer d(Tm)
  The abosolute value of the difference in melting temperatures
  between the forward and reverse primers. Ideally there should be
  little or no difference.

## Probe Tm
  The melting temperature (in degrees Celsius) of the hybridization
  probe.

## Min Primer Probe d(Tm)
  The difference between 'Probe Tm' and the max of 'FWD Tm' and 'REV
  Tm'. Ideally the hybridization probe should be 6 to 8 degrees
  Celsius higher than the primers' Tm.

## FWD %GC
  The GC-percentage of the forward primer.

## REV %GC
  The GC-percentage of the reverse primer.

## Probe %GC
  The GC-percentage of the hybridization probe.

## Amplicon %GC
  The GC-percentage of the amplicon sequence (for the reference
  genome).
  
## FWD 3' Stability
  From the Primer3 manual: "the delta G of disruption of the five 3'
  bases" of the forward primer.
  
## REV 3' Stability
  From the Primer3 manual: "the delta G of disruption of the five 3'
  bases" of the reverse primer.  

## Amplicon Size
  The length of the amplicon sequence (for the reference genome).

## Pair Penalty
  The penalty score for the primer pair, as reported by Primer3. Lower
  values are better.
  
## FWD Penalty
  The penalty score for the forward primer, as reported by
  Primer3. Lower values are better.
  
## REV Penalty
  The penalty score for the reverse primer, as reported by
  Primer3. Lower values are better.
  
## Probe Penalty
  The penalty score for the hybridization probe, as reported by
  Primer3. Lower values are better.
  
## FP IDs
  A comma-delimited list of NCBI Accession numbers of the false positive hits for the
  primer-probe set.

## FN IDs
  A comma-delimited list of NCBI Accession numbers of the false negative hits for the
  primer-probe set.


