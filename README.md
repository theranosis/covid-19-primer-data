# COVID-19 Primer Data #

This Git repository is a resource for validated and candidate (i.e.,
not yet validated) primer-probe sets, metadata about the context used
for creating the primers, and other related data. See [the original
blog
post](https://tomeraltman.net/2020/03/03/technical-problems-COVID-primers.html)
regarding the discovery of technical errors in the CDC's COVID-19
primer-probe sets for context regarding why this resource was put
together, and how specific files are used.

Please use the ["Issue" link](https://bitbucket.org/tomeraltman/covid-19-primer-data/issues?status=new&status=open) on the left-hand side to review existing
issues, and to add your own for reporting problems, or suggesting
improvements.

## Current Content

The following sections describe the corresponding sub-directories of this repository:

### `kit-primers`

Bioinformatic formats for currently-deployed primer-probe sets for
detecting COVID-19 ([currently available in FASTA format](https://bitbucket.org/tomeraltman/covid-19-primer-data/src/master/kit-primers/all-COVID-19-kit-primers.fasta)). The sequences and the identifiers for these
primers were obtained from the following sources:

* [CDC Laboratory Guidance](https://www.cdc.gov/coronavirus/2019-ncov/lab/rt-pcr-panel-primer-probes.html)
* [Comparative analysis of primer-probe sets for the laboratory confirmation of SARS-CoV-2](https://www.biorxiv.org/content/10.1101/2020.02.25.964775v1)
* [Detection of 2019 novel coronavirus (2019-nCoV) by real-time RT-PCR](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6988269/)

Also includes:

* `cdc-primers.fasta`: The CDC primer set. There are two primer sets (subset-1 and subset-2) for the N3 primer-probe set, because Primer3 cannot handle degeneracy.
* `cdc-primers-vs-nt-tsv.gz`: Tab-delimited NCBI Blast+ output with `cdc-primers.fasta` and the query, and NCBI NT database (downloaded 2020/03/04) as target.


## `primer-candidate-reports`

This directory contains spreadsheet report versions of the candidate primers for manual
inspection, in CSV and XLSX formats. The pipeline for generating these
candidate primers is described in the following [post](https://tomeraltman.net/2020/03/03/technical-problems-COVID-primers.html).

Current release (version 2) of improved and *in silico* validated COVID-19 candidate primers, ready for *in vitro* validation:

* [`new-COVID-19-primer-set-stats_v2.xlsx`](https://bitbucket.org/tomeraltman/covid-19-primer-data/src/master/primer-candidate-reports/new-COVID-primer-set-stats_v2.xlsx) 

It also includes the [raw output from Primer3](https://bitbucket.org/tomeraltman/covid-19-primer-data/src/master/primer-candidate-reports/primers.bio) in [Primer3 output BIO format](https://primer3.org/manual.html#outputTags). 

## `doc`

Contains documentation about the data files. Currently has a page describing the [semantics of the spreadsheet report columns](https://bitbucket.org/tomeraltman/covid-19-primer-data/src/master/doc/column-descriptions.md).

## `metadata`

This directory contains information used to generate the primers. See [the following post](https://tomeraltman.net/2020/03/03/technical-problems-COVID-primers.html) for more context on how these resources were used to generate the candidate primer pipeline. The files there include:

* `include-seqids.txt`: A list of COVID-19 complete genomes as obtained from the BLAST NT database.
* `exclude-seqids.txt`: A list of four non-COVID-19 but human-associated coronavirus complete genomes.
* `p3-new-COVID-19-parameters.txt`: The parameters (temperature & GC% constraints, minimal lengths, etc.) used with Primer3 in its ["Global" BIO format](https://primer3.org/manual.html#globalTags). This allows for others to use Primer3 to develop primers using the same settings.

## Contact

Please reach me on Twitter (@tomeraltman) or via email:
blog-at-me.tomeraltman.net. Thanks!